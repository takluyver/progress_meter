This provides a very simple progress bar for long running tasks.

The design is somewhat similar to `EasyGUI <http://easygui.sourceforge.net/>`_,
allowing simple scripts to be GUI-fied without being refactored into callbacks
and an event loop. It can naturally be used as an addition to the tools in
EasyGUI. The interface can't be quite as simple, but hopefully it's only a
little more complex.

progress_meter is based on `this code <http://tkinter.unpythonic.net/wiki/ProgressMeter>`_
by Michael Lange.

The high-level interface looks like this::

    from progress_meter import withprogress

    @withprogress(300, color="green")
    def demo(foo, bar=None):
        for i in range(300):
            # Do one (or a few) steps of processing, then...
            yield i
    
    demo()

You can use exceptions to see if the user cancelled the process before it
completed::

    try:
        demo()
    except UserCancelled:
        print("Cancelled")
    else:
        print("Completed")

There's also a lower-level interface in which you instantiate the window directly::

    from progress_meter import MeterWindow

    def _demostep(meter, value):
        meter.set(value)
        if value < 1.0:
            value = value + 0.005
            meter.after(50, lambda: _demostep(meter, value))
        else:
            meter.set(value, 'Demo successfully finished')

    def demo():
        root = MeterWindow(className='meter demo')
        root.meter.set(0.0, 'Starting demo...')
        root.after(1000, lambda: _demostep(root.meter, 0.0))
        root.mainloop()

To use the progress bar alone in a more complete GUI, you can use the ``Meter``
class from this module (which subclasses ``tkinter.Frame``).
